<?php
	require "./include/config.php";
	
	if (!isset($_SESSION['user_token'])){
		header('location:login.php');
		exit;
	}
	
	if (!isset($_SESSION['active'])){
		header('location:activate_account.php');
		exit;
	}
	
	//print_r($_SESSION);
	//exit;
	
    $smarty = new Smarty;

	$smarty->assign('title', 'Tokea | Events');
	$content = $smarty->fetch('./templates/events.tpl');
	$smarty->assign('content', $content);
	
	$smarty->display('./templates/main.tpl');

?>