<?php
	require "./include/config.php";
	
	
	if (isset($_SESSION['user_id'])) {
		unset($_SESSION);
	}
	
    $smarty = new Smarty;

	$smarty->assign('title', 'Tokea | Create Account');
	$content = $smarty->fetch('./templates/reg_user.tpl');
	$smarty->assign('content', $content);
	
	$smarty->display('./templates/user_reg.tpl');

?>