<!--=== Home Section Starts ===-->
		<div id="section-home" class="home-section-wrap center ">
			<div class="section-overlay"></div>
			<div class="container home bg-theme">
				<div class="row">
					<h2 class="well-come">Create Account</h2>
					
					<div class="confirmation alert alert-success">
						<p><span class="fa fa-check"></span></p>
					</div>
					
					
					<div class="col-md-12">
						
						<form class="contact-form support-form" id="register-form" method="post">
												
							<div class="col-lg-6">
								<div class="col-lg-12">
									<input id="fname" class="input-field form-item field-name" type="text" required="required" name="fname" placeholder="Your firstname" />
									<input id="lname" class="input-field form-item field-name" type="text" required="required" name="lname" placeholder="Your lastname" />
									<select id="gender" class="input-field form-item field-name" required="required" name="gender" >
										<option value="Female" selected="selected">Female</option>
										<option value="Male">Male</option>
									</select>
									<div>
										<input id="birthday" class="input-field form-item field-name" type="text" required="required" name="birthday" placeholder="Your birthday" />
									</div>
								</div>
							</div>
							
							<div class="col-lg-6">
								<div class="col-lg-12">
									<input id="address" class="input-field form-item field-name" type="text" required="required" name="address" placeholder="Your Hometown" />
									<input id="phone" class="input-field form-item field-name" type="phone" required="required" name="phone" placeholder="Your phone" />
									<input id="email" class="input-field form-item field-name" type="email" required="required" name="email" placeholder="Your email address" />
									
								</div>
							</div>	
							
							<div class="col-lg-12 col-sm-12">
								<div class="col-lg-6">
									
								</div>
								<div class="col-lg-6">
									<span class="pull-left">
									<input type="checkbox" name="agree" id="agree" required="required"> I agree to <a href="terms.php">Terms and Conditions</a>
								</span>
								</div>
							</div>
							
							<div class="col-lg-12 col-sm-12">
								<div class="col-lg-6">
									
								</div>
								
								<div class="col-lg-6">
									<button type="submit" class="btn btn-success zoom subform btn-block  margin-bottom" name="submit">
										Create Account
										<span class="icon">
											<i class="fa fa-sign-in"></i>
										</span>
									</button>
									<a class="btn btn-primary btn-block zoom">
										<span class="icon icon-fb">
											<i class="fa fa-facebook"></i>
										</span>
										Register With Facebook
									</a>
								</div>
							</div>
							
							
							<br>
							<br>
							
							
							
						</form>
						
					</div>
				</div>
			</div>
		</div>
		<!--=== Home Section Ends ===-->