<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<title>{$title}</title>
	
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/bootstrap.css" />
	<link rel="stylesheet" href="css/font-awesome.min.css" />
	<link rel="stylesheet" href="css/linea-icon.css" />
	<link rel="stylesheet" href="css/fancy-buttons.css" />
	
	<!--=== Google Fonts ===-->
	<link href='http://fonts.googleapis.com/css?family=Bangers' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300,700,400' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:600,400,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
	
	<!--=== Other CSS files ===-->
	<link rel="stylesheet" href="css/animate.css" />
	<link rel="stylesheet" href="css/jquery.vegas.css" />
	<link rel="stylesheet" href="css/baraja.css" />
	<link rel="stylesheet" href="css/jquery.bxslider.css" />
	<link rel="stylesheet" href="css/slider-style.css" />
	<link rel="stylesheet" type="text/css" href="css/date_picker.css">
	
	<!--=== Main Stylesheets ===-->
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/responsive.css" />
	
	<!--=== Color Scheme, three colors are available red.css, orange.css and gray.css ===-->
	<link rel="stylesheet" id="scheme-source" href="css/schemes/gray.css" />
	<link rel="shortcut icon" type="image/x-icon" href="images/logos/logo-100.png" />
	<!--=== Internet explorer fix ===-->
	<!-- [if lt IE 9]>
		<script src="http://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="http://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif] -->
	
	
</head>
<body>
	<!--=== Preloader section starts ===-->
	<section id="preloader">
		<div class="loading-circle fa-spin"> </div>
	</section>
	<!--=== Preloader section Ends ===-->
	
	<!--=== Header section Starts ===-->
	<div id="header" class="header-section">
		<!-- sticky-bar Starts-->
		<div class="sticky-bar-wrap">
			<div class="sticky-section">
				<div id="topbar-hold" class="nav-hold container">
					<nav class="navbar" role="navigation">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-responsive-collapse">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
							</button>
							<!--=== Site Name ===-->
							<a class="site-name navbar-brand" href="#"><img src="images/logos/title-logo-white.png"></a>
						</div>
						
						<!-- Main Navigation menu Starts -->
						<div class="collapse navbar-collapse navbar-responsive-collapse">
							<ul class="nav navbar-nav navbar-right">
								<li class=""><a href="index.php">Home</a></li>
								<li><a href="bunouts.php">BunOuts</a></li>
								{if (isset($smarty.session.user_token))}
								<li><a href="profile.php"><i class="glyphicon glyphicon-user"></i></a></li>
								<li><a href="msgs.php"><i class="fa fa-envelope"></i></a></li>
								{/if}
								<li><a href="create_event.php" ><span class="btn btn-success btn-create">Create event</span></a></li>
								{if (isset($smarty.session.user_token))}
									<li><a href="logout.php"><i class="fa fa-power-off"></i></a></li>
								{else}
									<li><a href="login.php"><span><img src="icons/png/glyphicons-4-user.png"/></span></a></li>
								{/if}
							</ul>
						</div>
						<!-- Main Navigation menu ends-->
					</nav>
				</div>
			</div>
		</div>
		<!-- sticky-bar Ends-->
		<!--=== Header section Ends ===-->
		
		{$content}
	</div>
	
	<!--=== Footer section Starts ===-->
	<div id="section-footer" class="footer-wrap">
		<div class="container footer center">
			<div class="row">
				<div class="col-lg-12">
					
					
					<!-- Social Links -->
					<p class="copyright">All rights reserved &copy; 2015</p>
				</div>
			</div>
		</div>
	</div>
	<!--=== Footer section Ends ===-->
	
<!--==== Js files ====-->
<!--==== Essential files ====-->
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="js/modernizr.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>

<!--==== Slider and Card style plugin ====-->
<script type="text/javascript" src="js/jquery.baraja.js"></script>
<script type="text/javascript" src="js/jquery.vegas.min.js"></script>
<script type="text/javascript" src="js/jquery.bxslider.min.js"></script>

<!--==== MailChimp Widget plugin ====-->
<script type="text/javascript" src="js/jquery.ajaxchimp.min.js"></script>

<!--==== Scroll and navigation plugins ====-->
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/jquery.nav.js"></script>
<script type="text/javascript" src="js/jquery.appear.js"></script>
<script type="text/javascript" src="js/jquery.fitvids.js"></script>

<!--==== Custom Script files ====-->
<script type="text/javascript" src="js/custom.js"></script>
<script src="js/jquery.wmuSlider.js"></script>
{literal}
<script type="text/javascript">
          
window.fbAsyncInit = function() {
  FB.init({
    appId : '676854189038131',
    cookie : true, // enable cookies to allow the server to access
    // the session
    xfbml : true, // parse social plugins on this page
    version : 'v2.0' // use version 2.0
  });
  FB.getLoginStatus(function(response) {
    if(response.status === 'connected'){
      //alert();
      FBuser();
    }
    else if (response.status === 'not_authorized') {
      $(".or").html('- OR -');
      $(".social-auth-links").html('<a href="#" onclick="FBlogin()" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using Facebook</a>');
    } else {
      $(".or").html('- OR -');
      $(".social-auth-links").html('<a href="#" onclick="FBlogin()" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using Facebook</a>');
    }
  });

};
( function(d) {
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {
      return;
    }
    js = d.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    ref.parentNode.insertBefore(js, ref);
  }(document));
function FBlogin() {
  FB.login(function(response) {
    if (response.authResponse) {
      location.href = "http://www.projectpal.co.ke/user_area/pages/user_reg.php";
    } 
  }, {
    scope : 'public_profile,email,user_friends, birthday,location'
  });
}

function logout() {
  FB.logout(function(reponse) {
    location.href = "signup.php";
  });
}

function FBuser() {

  var resp = document.getElementById("respdata");
  FB.api('/me?fields=id,first_name, last_name, birthday, location, email, address, locale, gender', function(response) {
  	//alert();  
  	//console.log(response);  
  	
  	/*for(var result in response) {
  		alert(response[result]);
  	} */
    var email = response.email;
    var dob = response.birthday;
    var image = 'https://graph.facebook.com/' + response.id + '/picture';
    var name = response.name;
    var firstname = response.first_name;
    var lastname = response.last_name;
    var gender = response.gender;
   //var location = response.location.name;
    
    //alert(response.birthday);
    
    
    $('#fname').val(firstname);
    $('#lname').val(lastname);
    $('#gender').val(gender);
    $('#address').val('');
    $('#bdate').val(dob);
    //$('#fbpic').val(image);
    $('#email').val(email);
    
    
  });
}


</script>


<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>

<script type="text/javascript">


  var placeSearch, autocomplete;
        function AddressInitialize(bounds) {
             // Create the autocomplete object, restricting the search
             // to geographical location types.
             autocomplete = new google.maps.places.Autocomplete((document.getElementById('address')),
               { types: ['geocode'] });
               if(bounds!==''){
                autocomplete.setBounds(bounds);   
               }
         }
         // Bias the autocomplete object to the user's geographical location,
         // as supplied by the browser's 'navigator.geolocation' object.

         //return bounds object
         function geolocate() {
             if (navigator.geolocation) {
                 navigator.geolocation.getCurrentPosition(function (position) {
                     var geolocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                     var circle = new google.maps.Circle({
                         center: geolocation,
                         radius: position.coords.accuracy
                     });
                     var bounds = circle.getBounds();
                     return bounds;
                 });
             }
        }
         AddressInitialize(geolocate());



</script>
{/literal}




{literal} 
<script>	
	 $('.example1').wmuSlider();         
</script> 

<script type="text/javascript" src="plugins/js/date_picker.js"></script>

<script type="text/javascript">
  $("#birthday").datepicker();
</script>
{/literal}
</body>
</html>