<!--=== Home Section Starts ===-->
<div id="section-home" class="home-section-wrap center ">
	<div class="section-overlay"></div>
	<div class="container home bg-theme">
		<div class="row">
			
			<!-- FIRST COLUMN-->
			<div class="col-lg-9">
				
				<div class="col-lg-12 article-title">
					<h3 >Upcoming Events</h3>
				</div>					
				<div class="col-lg-12">
					<div class="wmuSlider events">
						<div class="wrap">
						   
							 <article style="position: absolute; width:100%; opacity: 0;"> 
								<div class="cont-slider">
									<img src="images/events/2.jpg">
									<span class="u-slider-caption"><h4>Masaku Sevens</h4></span>
									<span class="u-slider-links">
										<ul>
											<li><a href="#" class="btn btn-success btn-sm">Details</a></li>
											<li><a href="#" class="btn btn-success btn-sm">Share</a></li>
											<li><a href="#" class="btn btn-success btn-sm">Attend</a></li>
										</ul>
									</span>
								</div>
							 </article>
							 <article style="position: absolute; width:100%; opacity: 0;"> 
								<div class="cont-slider">
									<img src="images/events/4.jpg">
									<span class="u-slider-caption"><h4>Mt. Kenya Safari Rally.</h4></span>
									<span class="u-slider-links">
										<ul>
											<li><a href="#" class="btn btn-success btn-sm">Details</a></li>
											<li><a href="#" class="btn btn-success btn-sm">Share</a></li>
											<li><a href="#" class="btn btn-success btn-sm">Attend</a></li>
										</ul>
									</span>
								</div>
							 </article>
							 <article style="position: absolute; width:100%; opacity: 0;"> 
								<div class="cont-slider">
									<img src="images/events/6.jpg">
									<span class="u-slider-caption toped"><h4>Carnivore Smirnoff Burnout.</h4></span><span class="u-slider-links">
									<span class="u-slider-links">
										<ul>
											<li><a href="#" class="btn btn-success btn-sm">Details</a></li>
											<li><a href="#" class="btn btn-success btn-sm">Share</a></li>
											<li><a href="#" class="btn btn-success btn-sm">Attend</a></li>
										</ul>
									</span>
								</div>
							 </article>
								
						 </div>
					 </div>	
				</div>
				
				<!-- SECOND COLUMN-->
				<div class="col-lg-12">
					<div class="panel panel-success">
						<div class="panel-heading">
							<ul class="main-links">
								<li><a href="#"><i class="fa fa-cog"></i></a></li>
								<li><a href="#"><i class="fa fa-user"></i></a></li>
								<li><a href="#"><i class="fa fa-group"></i></a></li>
								<li><a href="#"><i class="fa fa-link"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				
				<hr>
				
				<!--left-most Column-->
				<div class="col-lg-4">
				
				</div> 
				 
				<!--Middle Column -->
				<div class="col-lg-8">
					
					<div class="well panel-default"> 
						<form class="form-horizontal" role="form">
							<h5 class="align-left top-ten">What's Happening</h5>
							<div class="form-group" style="padding:1px;">
								<textarea class="form-control" placeholder="Update your status"></textarea>
							</div>
							<button class="btn btn-success btn-sm pull-right" type="button">Post</button>
							<ul class="list-inline align-left">
								<li><a href=""><i class="fa fa-camera"></i></a></li>
								<li><a href=""><i class="fa fa-map-marker"></i></a></li>
							</ul>
						</form>
					</div>
					
					<div class="panel panel-success">
						<div class="panel-heading"><em class="pull-right">12th June 2015</em> <h4 class="align-left top-ten"><img src="images/users/2.jpg" class="img-circle user-icon1">Uweys Salim</h4></div>
						<div class="panel-body">
							<img src="images/posts/2.jpg" class="">
							<div class="clearfix"></div>
							<ul class="post-links">
								<li><a href="#">Like</a></li>
								<li><a href="#">Comment</a></li>
								<li><a href="#">Share</a></li>
							</ul>
							<hr>
							<p class="align-left">That was one awesome turnup, with <code>Timo Gaita</code>, <code>Sammy Mutisya</code>, <code>Fred Fosi</code> at <a href="#">Crayfish Naivasha</a></p>
							
							<div class="alert alert-success comments-div">
								<p class="align-left">
									
									<img src="images/users/2.jpg" class="pull-left img-icon1">
									That was one awesome turnup, with
									
								</p>
								<p class="align-left">That was one awesome turnup, with</p>
								<p class="align-left">That was one awesome turnup, with</p>
							</div>
						
						</div>
						<div class="panel-footer">
							<form>
								<div class="input-group">
									<div class="input-group-btn">
										<a class="btn btn-default cmt"><img src="images/users/2.jpg" class=""></a>
									</div>
									<input class="form-control cmt" placeholder="Add a comment.." type="text">
								</div>
							</form>
						</div>
					</div>
					
					<div class="panel panel-success">
						<div class="panel-heading"><a href="#" class="pull-right">View Posts</a> <h4 class="align-left top-ten">Fred Fosi</h4></div>
						<div class="panel-body">
							<img src="images/posts/2.jpg" class="">
							<div class="clearfix"></div>
							<hr>
							<p class="align-left">That was one awesome turnup, with <code>Timo Gaita</code>, <code>Sammy Mutisya</code>, <code>Fred Fosi</code> at <a href="#">Crayfish Naivasha</a></p>
							<hr>
							<form>
								<div class="input-group">
									<div class="input-group-btn">
										<a class="btn btn-default cmt"><img src="images/users/3.jpg" class=""></a>
									</div>
									<input class="form-control cmt" placeholder="Add a comment.." type="text">
								</div>
							</form>
						</div>
					</div>
					
				</div>
				
				
			</div>
			
			<div class="col-lg-3">
			</div>
			
				
			
		</div>
	</div>
</div>