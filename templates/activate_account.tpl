<!--=== Home Section Starts ===-->
		<div id="section-home" class="home-section-wrap center ">
			<div class="section-overlay"></div>
			<div class="container home bg-theme">
				<div class="row">
					<h2 class="well-come">Activate Account</h2>
					<div class="alert alert-success">
						<p>A Text message was sent to you with a password. Enter the code here</p>
					</div>
					
					
					
					<div class="col-md-6 col-md-offset-3">
						<div class="confirmation alert alert-info">
							<p><span class="fa fa-check"></span></p>
						</div>
						
						<form class="contact-form support-form" id="activate-form" method="post">
								<div class="col-md-12">
									<h4>Welcome {$smarty.session.user_name}</h4>
									<input id="password" class="input-field form-item field-name" type="text" required="required" name="password" placeholder="Your Password" />
									
								</div>
							
								<div class="col-md-12">
									
									<button type="submit" class="btn btn-success zoom subform btn-block  margin-bottom" name="submit">
										Activate
										<span class="icon">
											<i class="fa fa-sign-in"></i>
										</span>
									</button>
									<a class="btn btn-primary btn-block zoom">
										Resend code
										<span class="icon">
											<i class="fa fa-lock"></i>
										</span>
									</a>
									
									<h4><a href="reg_user.php">Create a new account</a></h4>
								</div>
							
						</form>
						
					</div>
				</div>
			</div>
		</div>
		<!--=== Home Section Ends ===-->