<?php

class classMain {
    
	var $host;               // mySQL host to connect to
    var $user;               // mySQL user name
    var $pw;                 // mySQL password
    var $db;                 // mySQL database to select
	
	var $last_error;            // current/last database link identifier
    
    var $db_link;            // current/last database link identifier
    var $auto_slashes;       // the class will add/strip slashes when it can
	var $currentYear;
	
	var $error_verify;
	
	var $users;
	var $count_users;
	var $unread_count;
	var $unread_messages;
	var $count_all_messages;
	
    private $username = "patikana";
    private $apiKey = "25dee3389d84101b3b7da93f0d6683d3af7e1fcbe46263c4ba09f97f0e869fe2";
 
	
    var $cypher="sha256";    //password
    
    function __construct() { 
		
		$this->connect();
    }

   function connect($host=HOST, $user=USER, $pw=PASS, $db=DBASE, $persistant=true) {
		date_default_timezone_set('Africa/Nairobi');
		$this->currentYear = date('Y', time()); 
		if (!empty($host)) $this->host = $host; 
		if (!empty($user)) $this->user = $user; 
		if (!empty($pw)) $this->pw = $pw; 

		// Establish the connection.
		if ($persistant) 
		 $this->db_link = mysql_pconnect($this->host, $this->user, $this->pw);
		else 
		 $this->db_link = mysql_connect($this->host, $this->user, $this->pw);

		// Check for an error establishing a connection
		if (!$this->db_link) {
		 $this->last_error = mysql_error();
		 return false;
		} 

		// Select the database
		if (!$this->select_db($db)) //return false;

		return $this->db_link;  // success
   }

   function select_db($db='') {
      // Selects the database for use.  If the function's $db parameter is 
      // passed to the function then the class variable will be updated.
 
      if (!empty($db)) $this->db = $db; 
     
      if (!mysql_select_db($this->db)) {
         $this->last_error = mysql_error();
         return false;
      }
 
      return true;
    }

	function check_user($email, $user_level) {
		switch ($user_level) {
            case 1: 
            $sql = "SELECT COUNT(*) AS num_rows FROM users WHERE email = '".$email."'";
            break;
			case 2: 
            $sql = "SELECT COUNT(*) AS num_rows FROM users WHERE email = '".$email."' AND user_level='2'";
            break;
			default:
            $sql = "SELECT COUNT(*) AS num_rows FROM users WHERE email = '".$email."'";
		}	
        $result = mysql_query($sql) or die('Check error=>'.mysql_error());
        
        if (mysql_result($result, 0, "num_rows") == 1) {
            return true;
        } else {
            return false;
        }
    }  
	
	function check_user_phone($phone, $user_level) {
		switch ($user_level) {
            case 1: 
            $sql = "SELECT COUNT(*) AS num_rows FROM users WHERE phone = '".$phone."'";
            break;
			default:
            $sql = "SELECT COUNT(*) AS num_rows FROM users WHERE phone = '".$phone."'";
			
		}	
        $result = mysql_query($sql) or die('Check error=>'.mysql_error());
        
        if (mysql_result($result, 0, "num_rows") == 1) {
            return true;
        } else {
            return false;
        }
    }

	
	function generate_verify_code ($l, $c) {
		
		for ($s = '', $cl = strlen($c)-1, $i = 0; $i < $l; $s .= $c[mt_rand(0, $cl)], ++$i);
		return $s;

	}
	
	public function generatePassword($password) {
		$hash = password_hash($password, PASSWORD_DEFAULT);
        return $hash;
    }
	
	public function comparePassword($pass, $db_hash) {
   		return (password_verify($pass, $db_hash)) ? true : false; 
    }
    
    public function password_verify_with_rehash($pass, $db_hash, $db_id) {
		    if (!password_verify($pass, $db_hash)) {
		        return false;
				exit;
		    }
		
		    if (password_needs_rehash($db_hash, PASSWORD_DEFAULT)) {
		        $hash = password_hash($pass, PASSWORD_DEFAULT);
		        
		        $sql = "UPDATE `users` SET password ='".$pass."' WHERE id='".$db_id."'";
				$res = mysql_query($sql) or die(mysql_error());
			}
		
		    return true;
	}
	
	function register_user($fname, $lname, $dob, $gender, $phone, $email, $pword, $address, $accessLevel) {
		
			if ($this->check_user_phone($phone, $accessLevel)) {
				return 77;
				exit;
			} else if ($this->check_user($email, $accessLevel)) {
				return 78;
				exit;
			} 
			
			$thumb = isset($_SESSION['thumb'])? $_SESSION['thumb']:'user.png';
			$now = date ('d/m/Y H:i:s');
			$secure_password = $this->generatePassword($pword);
			$verify_code = $pword;
			//echo $verify_code;
			//exit;
			
			$sql = "INSERT INTO users
					SET 
						user_token='".$verify_code."',
						access_level='".$accessLevel."',
						pword='".$pword."',
						fname='".$fname."',
						lname='".$lname."',
						phone='".$phone."',
						dob='".$dob."',
						gender='".$gender."',
						email='".$email."',
						password='".$secure_password."',
						address='".$address."',
						active=0,
						date_registered='".$now."'" ;
	
			$res = mysql_query($sql) or die(mysql_error()); 
	
			if($res) {
					$user_id = mysql_insert_id();
					
				
					$_SESSION['user_id'] = $user_id;
					$_SESSION['user_token'] = $verify_code;
					$_SESSION['access_level'] = $accessLevel;
					$_SESSION['user_name'] = $fname." ".$lname;
					$_SESSION['firstname'] = $fname;
					$_SESSION['lastname'] = $lname;
					$_SESSION['user_email'] = $email;
					$_SESSION['user_phone'] = $phone;
					$_SESSION['user_address'] = $address;
					$_SESSION['thumbnail'] = $thumb;
					$_SESSION['active'] = 0;
					
					//if (isset($_SESSION['thumb'])) unset($_SESSION['thumb']);
					//$this->send_confirmation_text($phone, $pword, '');
					
					return true;
										
			} else {
				return false;
			}
	}
	
	
	function user_login ($email, $pass, $user_level) {
		$arr = array ();
		
		if($this->check_user($email, $user_level)) {
			
			$user_data = $this->fetch_pass($email);
			
			$db_hash = $user_data['password'];
			$db_id = $user_data['id'];
			
			if ($this->password_verify_with_rehash($pass, $db_hash, $db_id)) {
				
				//print_r('<pre>');
				//print_r($user_data);
				if ($user_level == 2) {
					$_SESSION['admin'] = $user_data['email'];
					$_SESSION['admin_id'] = $user_data['id'];
					$_SESSION['admin_name'] = $user_data['fname']." ".$user_data['lname'];
				}	
				$_SESSION['user_id'] = $user_data['id'];
				$_SESSION['user_name'] = $user_data['fname']." ".$user_data['lname'];
				$_SESSION['firstname'] = $user_data['fname'];
				$_SESSION['lastname'] = $user_data['lname'];
				$_SESSION['user_token'] = $user_data['user_token'];
				$_SESSION['user_email'] = $user_data['email'];
				$_SESSION['user_phone'] = $user_data['phone'];
				$_SESSION['user_address'] = $user_data['address'];
				$_SESSION['thumbnail'] = $user_data['thumbnail'];
				$_SESSION['active'] = $user_data['active'];
				
				return true;
				 
			} else {
				return false;
			}
		} else {
			return false;
		}		
    }

	function activate_account($id, $code) {
		
		$user_data = $this->get_user_info_by_id($id);
		
					
		if($this->check_user_phone($_SESSION['user_phone'], 1)) {
			
			$user_data = $this->fetch_pass($_SESSION['user_email']);
			
			$db_hash = $user_data['password'];
			$db_id = $user_data['id'];
			
			if ($this->password_verify_with_rehash($code, $db_hash, $db_id)) {
				
				$sql = "UPDATE `users` 
							SET
								active= 1 
							WHERE id='".$id."'";
				
				$res = mysql_query($sql) or die(mysql_error());
				if ($res) {
					//unset($_SESSION);
					$_SESSION['user_id'] = $id;
					$_SESSION['user_name'] = $user_data['fname']." ".$user_data['lname'];
					$_SESSION['firstname'] = $user_data['fname'];
					$_SESSION['lastname'] = $user_data['lname'];
					$_SESSION['user_email'] = $user_data['email'];
					$_SESSION['user_phone'] = $user_data['phone'];
					$_SESSION['user_address'] = $user_data['address'];
					$_SESSION['user_token'] = $user_data['user_token'];
					$_SESSION['thumbnail'] = $user_data['thumbnail'];
					$_SESSION['active'] = 1;
					
					return true;
				} else {
					
					$this->error_verify = 'Authentification failed, check verification code';
					return false;
				}
			
			} else {
				
				return false;
			}
		} else {
			return false;
		}	
		
	}
	
	function user_update_profile($id, $fname, $lname, $dob, $gender, $email,$phone, $address, $id_no, $bank_name, $bank_branch, 
			$bank_account_number) {
		
			if ($this->check_user_phone_update($phone, $id)) {
				return 77;
				exit;
			} else if ($this->check_user_update($email, $id)) {
				return 78;
				exit;
			} 
			
			$now = date ('d/m/Y H:i:s');
			//$secure_password = $this->generatePassword($pword);
			//$verify_code = $this->generate_verify_code ('5', '0123456789');
			//echo $verify_code;
			//exit;
			
			
			$sql = "UPDATE users
					SET 
						fname='".$fname."',
						lname='".$lname."',
						phone='".$phone."',
						email='".$email."',
						dob='".$dob."',
						gender='".$gender."',
						id_no='".$id_no."',
						bank_name='".$bank_name."',
						bank_branch='".$bank_branch."',
						bank_account_number='".$bank_account_number."', 
						address='".$address."',
						last_update='".$now."'
					WHERE
						id='".$id."'" ;
	
			$res = mysql_query($sql) or die(mysql_error()); 
	
			if($res) {
					
				    $user_data = $this->getUserDataByID($id);
				
					$_SESSION['user_id'] = $user_data['id'];
					$_SESSION['user_name'] = $user_data['fname']." ".$user_data['lname'];
					$_SESSION['firstname'] = $user_data['fname'];
					$_SESSION['lastname'] = $user_data['lname'];
					$_SESSION['user_code'] = $user_data['verify_code'];
					$_SESSION['user_email'] = $user_data['email'];
					$_SESSION['user_phone'] = $user_data['phone'];
					$_SESSION['user_address'] = $user_data['address'];
					$_SESSION['thumbnail'] = $user_data['thumbnail'];
					$_SESSION['user_folder_name'] = $user_data['user_folder_name'];
					$_SESSION['active'] = $user_data['active'];
					
					return true;
										
			} else {
				return false;
			}
	}

	function set_profile_pic($id, $thumb) {
		
			
			$now = date ('d/m/Y H:i:s');
			$sql = "UPDATE users
					SET 
						thumbnail='".$thumb."',
						last_update='".$now."'
					WHERE
						id='".$id."'" ;
	
			$res = mysql_query($sql) or die(mysql_error()); 
			
			if($res) {
				
				$_SESSION['thumbnail'] = $thumb;
				return true;
										
			} else {
				return 309;
			}
	}
	
	function reset_password ($phone, $level) {
		$phone = intval($phone);
		
		$code = $this->generate_verify_code ('5', '0123456789');
			
		if ($this->save_reset_password ($phone, $code)) {
			$this->sendText($phone, $code, true);
			return true;
		} else {
			return false;
		}
		
	}	
	
	function save_reset_password ($phone, $code) {
		$secure_password = $this->generatePassword($code);
		$sql = "UPDATE users
					SET 
						verify_code='".$code."',
						password='".$secure_password."'
					WHERE 
						phone='".$phone."'" ;
		$res = mysql_query($sql) or die(mysql_error()); 
	
		if($res) {
			return true;
		} else {
			return false;
		}
		
	}	
	
	function send_confirmation_text($recipient, $pword) {
		$msg = "Welcome to Tokea. Your password is: ".$pword;
        
		$res = $this->send_text($recipient, $msg);
	}
		
	function send_text($recipients, $msg) {
        	
        $rcp=intval($recipients);	  
		$gateway = new AfricasTalkingGateway($this->username, $this->apiKey);
		$results = $gateway -> sendMessage($rcp, $message);
		
		return true;
	}
	
	
	function send_mail($mail_address, $msg = 29, $subj = 28, $send_admin = false) {
	    $mail = new SimpleMail();
        if($mail->send_mail($mail_address, $msg, $subj, $send_admin)) {
            return true;
        }
    }
	
	
	
	
	
    
    function check_user_update($email, $id) {
		$sql = "SELECT COUNT(*) AS num_rows FROM users WHERE email = '".$email."' AND id!='".$id."'";
        $result = mysql_query($sql) or die('Check error=>'.mysql_error());
        
        if (mysql_result($result, 0, "num_rows") == 1) {
            return true;
        } else {
            return false;
        }
    }  
	
	function check_user_phone_update($phone, $id) {
		$sql = "SELECT COUNT(*) AS num_rows FROM users WHERE phone = '".$phone."' AND id!='".$id."'";
        $result = mysql_query($sql) or die('Check error=>'.mysql_error());
        
        if (mysql_result($result, 0, "num_rows") == 1) {
            return true;
        } else {
            return false;
        }
    }             
    
	public function fetch_pass($email) {
        
		$sql = null;
        $sql = "SELECT *
					FROM `users` 
					WHERE `email` = '".$email."'";
        if (!$result = mysql_query($sql)) {
           return false;
        } else {
            $row = mysql_fetch_array($result);
            return $row; 
        }   
        
    }
	
	
	public function get_user_info_by_id($id){
        
		$sql = null;
        $sql = "SELECT *
					FROM `users` 
					WHERE `id` = '".$id."'";
        if (!$result = mysql_query($sql)) {
           return false;
        } else {
            $row = mysql_fetch_array($result);
            return $row; 
        }   
        
    }
	
	function get_user_profile($id) {
		$arr_all =array ();
		$arr = array();
		
		$sql ="SELECT * FROM `users` WHERE id='".$id."'";
        $res = mysql_query($sql) or die(mysql_error());
       
	    if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $arr [] = $row;
            }   
			
			return $arr;    
        } else {
			return 0;
	    }
       
	   
	}
	function get_users() {
		$arr_all =array ();
		$arr = array();
		$sql =" SELECT * FROM `users` ORDER BY id DESC";
        $res = mysql_query($sql) or die(mysql_error());
		
		if($res) {
            while($row = mysql_fetch_assoc($res)) {
            	$arr[] = $row;
            }   
        }
		
		$this->count_users = sizeof($arr);
		
        return $arr;
		  
    }
	
	
	
	
	function subscribe($email) {
		$now = date ('d/m/Y H:i:s');
		$ip = 0;
		//echo $now;
		$sql = "SELECT * FROM subscription WHERE email='".$email."'";
		$res = mysql_query($sql) or die('subscribe=>'.mysql_error());
		
		if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $db_email = $row ['email'];
			}   
			
			if ($db_email!=$email) {
				$this->complete_subscription($email);
			} else {
				return 11;
			}
			
        } else {
			return 0;
	    }
       
    }
		
	function complete_subscription($email){
		$now = date ('d/m/Y H:i:s');
		$sql = "INSERT INTO `subscription`
						SET
							`email` = '".$email."',
							`ip` = '".$ip."',
							`date_saved` = '".$now."'";
							
		$res = mysql_query($sql) or die('subscribe=>'.mysql_error());
		
		if ($res) {
			return 1;
		} else {
			return 0;		
		}
	}
    
    function logout() {
		session_destroy();
		header('location:./');
    }
    
   }