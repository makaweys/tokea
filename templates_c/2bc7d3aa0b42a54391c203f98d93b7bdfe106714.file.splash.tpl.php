<?php /* Smarty version Smarty-3.1.14, created on 2015-05-25 00:16:31
         compiled from ".\templates\splash.tpl" */ ?>
<?php /*%%SmartyHeaderCode:157915560607b09ff21-57490434%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2bc7d3aa0b42a54391c203f98d93b7bdfe106714' => 
    array (
      0 => '.\\templates\\splash.tpl',
      1 => 1432505788,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '157915560607b09ff21-57490434',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5560607b21c0e1_73192460',
  'variables' => 
  array (
    'title' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5560607b21c0e1_73192460')) {function content_5560607b21c0e1_73192460($_smarty_tpl) {?><!DOCTYPE HTML>
<html lang="en-US">
<head>
	<title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
	
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/bootstrap.css" />
	<link rel="stylesheet" href="css/font-awesome.min.css" />
	<link rel="stylesheet" href="css/linea-icon.css" />
	<link rel="stylesheet" href="css/fancy-buttons.css" />
	
	<!--=== Google Fonts ===-->
	<link href='http://fonts.googleapis.com/css?family=Bangers' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300,700,400' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:600,400,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
	
	<!--=== Other CSS files ===-->
	<link rel="stylesheet" href="css/animate.css" />
	<link rel="stylesheet" href="css/jquery.vegas.css" />
	<link rel="stylesheet" href="css/baraja.css" />
	<link rel="stylesheet" href="css/jquery.bxslider.css" />
	<link rel="stylesheet" href="css/slider-style.css" />
	
	<!--=== Main Stylesheets ===-->
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/responsive.css" />
	
	<!--=== Color Scheme, three colors are available red.css, orange.css and gray.css ===-->
	<link rel="stylesheet" id="scheme-source" href="css/schemes/gray.css" />
	<link rel="shortcut icon" type="image/x-icon" href="images/logos/logo-100.png" />
	<!--=== Internet explorer fix ===-->
	<!-- [if lt IE 9]>
		<script src="http://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="http://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif] -->
	
	
</head>
<body>
	<!--=== Preloader section starts ===-->
	<section id="preloader">
		<div class="loading-circle fa-spin"> </div>
	</section>
	<!--=== Preloader section Ends ===-->
	
	<!--=== Header section Starts ===-->
	<div id="header" class="header-section">
		
		<!--=== Header section Ends ===-->
		
		<!--=== Home Section Starts ===-->
		<div id="section-home" class="home-section-wrap center">
			<div class="section-overlay splash-overlay"></div>
			<div class="container home">
				<div class="row">
					<span class="well-come home-well"><img src="images/logos/logo-200.png"></span>
					<h2 class="home-title">Tokea</h2>
					<p class="slang">Turn Up . Connect . Share</p>
					
					
					<div class="col-md-8 col-md-offset-2 btn-area">
						<div class="col-md-8 col-md-offset-2 ">
						
							<div class="row">
								<div class="col-md-12 theme-bg">
									<h3>Sign In</h3>
									<div class="confirmation alert alert-success">
										<p><span class="fa fa-check"></span></p>
									</div>
									<form class="contact-form support-form" id="login-form">
									
										<div class="col-lg-12">
											<input id="email" class="input-field form-item field-email" type="email" required="required" name="email" placeholder="Email" />
											<input id="password" class="input-field form-item field-name" type="password" required="required" name="password" placeholder="******" />
										</div>
										<div class="col-lg-12 col-sm-12">
											<span class="pull-left">
												<input type="checkbox" name="agree" id="agree" required="required"> I agree to <a href="terms.php">Terms and Conditions</a>
											</span>
											<button type="submit" class="btn btn-primary zoom subform btn-block">
												Login
												<span class="icon">
													<i class="fa fa-sign-in"></i>
												</span>
											</button>
											<a href="reg_user.php" class="btn btn-success zoom btn-block  margin-bottom">
												Create Account
												<span class="icon">
													<i class="fa fa-user"></i>
												</span>
											</a>
										</div>
										
										
									</form>
									
									
									
										<div class="col-lg-12 col-sm-12">
										
										
									</div>
								</div>
								
							</div>	
							
						</div>
						
						
							
						
					</div>
					
					
				</div>
			</div>
		</div>
		<!--=== Home Section Ends ===-->
	</div>
	
	<!--=== Footer section Starts ===-->
	<!--<div id="section-footer" class="footer-wrap">
		<div class="container footer center">
			<div class="row">
				<div class="col-lg-12">
					
					
					
					<p class="copyright">All rights reserved &copy; 2015</p>
				</div>
			</div>
		</div>
	</div> -->
	<!--=== Footer section Ends ===-->
	
<!--==== Js files ====-->
<!--==== Essential files ====-->
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="js/modernizr.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>

<!--==== Slider and Card style plugin ====-->
<script type="text/javascript" src="js/jquery.baraja.js"></script>
<script type="text/javascript" src="js/jquery.vegas.min.js"></script>
<script type="text/javascript" src="js/jquery.bxslider.min.js"></script>

<!--==== MailChimp Widget plugin ====-->
<script type="text/javascript" src="js/jquery.ajaxchimp.min.js"></script>

<!--==== Scroll and navigation plugins ====-->
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/jquery.nav.js"></script>
<script type="text/javascript" src="js/jquery.appear.js"></script>
<script type="text/javascript" src="js/jquery.fitvids.js"></script>

<!--==== Custom Script files ====-->
<script type="text/javascript" src="js/custom.js"></script>
<script src="js/jquery.wmuSlider.js"></script>
 
<script>
     	
	 $('.example1').wmuSlider();         
</script> 

</body>
</html><?php }} ?>