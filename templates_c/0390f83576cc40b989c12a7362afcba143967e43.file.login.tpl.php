<?php /* Smarty version Smarty-3.1.14, created on 2015-05-23 00:33:56
         compiled from ".\templates\login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2485555faed4a68fd6-04438935%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0390f83576cc40b989c12a7362afcba143967e43' => 
    array (
      0 => '.\\templates\\login.tpl',
      1 => 1432330874,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2485555faed4a68fd6-04438935',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_555faed4b45d88_79444076',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_555faed4b45d88_79444076')) {function content_555faed4b45d88_79444076($_smarty_tpl) {?><!--=== Home Section Starts ===-->
		<div id="section-home" class="home-section-wrap center ">
			<div class="section-overlay"></div>
			<div class="container home bg-theme">
				<div class="row">
					<h1 class="well-come">Sign In</h1>
					
					
					
					<div class="col-md-6 col-md-offset-3">
						<form class="contact-form support-form" id="register-form">
						
							<div class="col-lg-12">
								<input id="email" class="input-field form-item field-email" type="email" required="required" name="contact-email" placeholder="Email" />
								<input id="password" class="input-field form-item field-name" type="password" required="required" name="password" placeholder="******" />
							</div>
							<div class="col-lg-12 col-sm-12">
								<span class="pull-left">
									<input type="checkbox" name="agree" id="agree" required="required"> I agree to <a href="terms.php">Terms and Conditions</a>
								</span>
								<button type="submit" class="pull-right fancy-button button-line button-white zoom subform">
									Login
									<span class="icon">
										<i class="fa fa-sign-in"></i>
									</span>
								</button>
							</div>
							<br>
							<br>
							<br>
							<br>
							<div class="col-lg-12 col-sm-12">
								<a class="btn btn-primary btn-block">
									<span class="icon">
										<i class="fa fa-facebook"></i>
									</span>
									Register With Facebook
								</a>
								<a href="register.php" class="btn btn-success btn-block">
									Create an Account
								</a>
							</div>
							
						</form>
						
					</div>
				</div>
			</div>
		</div>
		<!--=== Home Section Ends ===--><?php }} ?>