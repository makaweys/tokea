<?php /* Smarty version Smarty-3.1.14, created on 2015-05-24 23:53:27
         compiled from ".\templates\activate_account.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1948055623d07f3fd65-84946552%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8c0f7fe0e4255049b573d8081557db68716f668f' => 
    array (
      0 => '.\\templates\\activate_account.tpl',
      1 => 1432504404,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1948055623d07f3fd65-84946552',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_55623d0830c1f4_78261988',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55623d0830c1f4_78261988')) {function content_55623d0830c1f4_78261988($_smarty_tpl) {?><!--=== Home Section Starts ===-->
		<div id="section-home" class="home-section-wrap center ">
			<div class="section-overlay"></div>
			<div class="container home bg-theme">
				<div class="row">
					<h2 class="well-come">Activate Account</h2>
					<div class="alert alert-success">
						<p>A Text message was sent to you with a password. Enter the code here</p>
					</div>
					
					
					
					<div class="col-md-6 col-md-offset-3">
						<div class="confirmation alert alert-info">
							<p><span class="fa fa-check"></span></p>
						</div>
						
						<form class="contact-form support-form" id="activate-form" method="post">
								<div class="col-md-12">
									<h4>Welcome <?php echo $_SESSION['user_name'];?>
</h4>
									<input id="password" class="input-field form-item field-name" type="text" required="required" name="password" placeholder="Your Password" />
									
								</div>
							
								<div class="col-md-12">
									
									<button type="submit" class="btn btn-success zoom subform btn-block  margin-bottom" name="submit">
										Activate
										<span class="icon">
											<i class="fa fa-sign-in"></i>
										</span>
									</button>
									<a class="btn btn-primary btn-block zoom">
										Resend code
										<span class="icon">
											<i class="fa fa-lock"></i>
										</span>
									</a>
									
									<h4><a href="reg_user.php">Create a new account</a></h4>
								</div>
							
						</form>
						
					</div>
				</div>
			</div>
		</div>
		<!--=== Home Section Ends ===--><?php }} ?>