<?php /* Smarty version Smarty-3.1.14, created on 2015-05-25 01:44:08
         compiled from ".\templates\events.tpl" */ ?>
<?php /*%%SmartyHeaderCode:114845562612d8ae828-93398170%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd62bf4ca95f53d5aa4f7c9f7ec6d0423fbc27e1d' => 
    array (
      0 => '.\\templates\\events.tpl',
      1 => 1432511033,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '114845562612d8ae828-93398170',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5562612d906c47_84622387',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5562612d906c47_84622387')) {function content_5562612d906c47_84622387($_smarty_tpl) {?><!--=== Home Section Starts ===-->
<div id="section-home" class="home-section-wrap center ">
	<div class="section-overlay"></div>
	<div class="container home bg-theme">
		<div class="row">
			<div class="col-lg-12">
				<div class="col-md-12">
					
					<div class="col-md-6">
						<h2>Upcoming Events</h2>
					</div>
					<div class="col-md-6">
						
					</div>
				</div>
				
				
				<!-- Events -->
						<div id="works"  class=" clearfix grid"> 
						    <figure class="effect-oscar  wowload fadeInUp">
						        <img src="images/events/1.jpg" alt="img01"/>
						        <figcaption>
						            <h3>Nature Event</h3>
						            <p>Enjoy time in the wilderness<br>
						            <a href="images/events/1.jpg"  data-gallery>View more details</a></p>            
						        </figcaption>
						    </figure>
						     <figure class="effect-oscar  wowload fadeInUp">
						        <img src="images/events/2.jpg" alt="img01"/>
						        <figcaption>
						            <h3>City Events</h3>
						            <p>Most Entertaining party 2015<br>
						            <a href="images/events/2.jpg" title="1" data-gallery>View more</a></p>            
						        </figcaption>
						    </figure>
						     <figure class="effect-oscar  wowload fadeInUp">
						        <img src="images/events/3.jpg" alt="img01"/>
						        <figcaption>
						            <h3>Music concert</h3>
						            <p>Lily likes to play with on stage<br>
						            <a href="images/events/3.jpg" title="1" data-gallery>View more</a></p>            
						        </figcaption>
						    </figure>
						     <figure class="effect-oscar  wowload fadeInUp">
						        <img src="images/events/4.jpg" alt="img01"/>
						        <figcaption>
						            <h3>Vintage</h3>
						            <p>Lily likes to play with crayons and pencils<br>
						            <a href="images/events/4.jpg" title="1" data-gallery>View more</a></p>            
						        </figcaption>
						    </figure>
						     <figure class="effect-oscar  wowload fadeInUp">
						        <img src="images/events/5.jpg" alt="img01"/>
						        <figcaption>
						            <h3>Typers</h3>
						            <p>Lily likes to play with crayons and pencils<br>
						            <a href="images/events/5.jpg" title="1" data-gallery>View more</a></p>            
						        </figcaption>
						    </figure>
						     
						     <figure class="effect-oscar  wowload fadeInUp">
						        <img src="images/events/6.jpg" alt="img01"/>
						        <figcaption>
						            <h3>hotel</h3>
						            <p>Lily likes to play with crayons and pencils<br>
						            <a href="images/events/6.jpg" title="1" data-gallery>View more</a></p>            
						        </figcaption>
						    </figure>
						    <figure class="effect-oscar  wowload fadeInUp">
						        <img src="images/events/7.jpg" alt="img01"/>
						        <figcaption>
						            <h3>Chinese Foods Exhibition</h3>
						            <p>Lily likes to play with crayons and pencils<br>
						            <a href="images/events/7.jpg" title="1" data-gallery>View more</a></p>            
						        </figcaption>
						    </figure>
						    <figure class="effect-oscar  wowload fadeInUp">
						        <img src="images/events/8.jpg" alt="img01"/>
						        <figcaption>
						            <h3>Dicrap Events</h3>
						            <p>Lily likes to play with crayons and pencils<br>
						            <a href="images/events/8.jpg" title="1" data-gallery>View more</a></p>            
						        </figcaption>
						    </figure>
						    <figure class="effect-oscar  wowload fadeInUp">
						        <img src="images/events/9.jpg" alt="img01"/>
						        <figcaption>
						            <h3>Coffee Talks</h3>
						            <p>Lily likes to play with crayons and pencils<br>
						            <a href="images/events/9.jpg" title="1" data-gallery>View more</a></p>            
						        </figcaption>
						    </figure>
						    
						    
						</div>
						<!-- works -->
				 
			 </div> 	
			
		</div>
	</div>
</div>
<!--=== Home Section Ends ===-->

<?php }} ?>