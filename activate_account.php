<?php
	require "./include/config.php";
	
	if (!isset($_SESSION['user_token'])){
		header('location:login.php');
		exit;
	}
	 //print_r($_SESSION);
	 //exit;
	
    $smarty = new Smarty;

	$smarty->assign('title', 'Tokea | Activate Account');
	$content = $smarty->fetch('./templates/activate_account.tpl');
	$smarty->assign('content', $content);
	
	$smarty->display('./templates/main.tpl');

?>